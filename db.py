import os
import psycopg2

class Conexion:
    conexion = None
    cursor = None

    def __init__(self, database, user, password, port, host):
        
        self.database = database
        self.user = user
        self.password = password
        self.port = port
        self.host = host

        self.enlace()

    def enlace(self):
                
        self.conexion = psycopg2.connect(database = self.database, user = self.user, password = self.password, port = self.port, host = self.host)
        self.cursor = self.conexion.cursor()
        
        # Ejecuta la sentencia SQL
        # self.cursor.execute("SELECT * FROM representantes;")

        # Obtiene el resultado de la sentencia en una variable
        # resultado = self.cursor.fetchall()
        # print("ESTE ES EL RESULTADODE LA PRIMERA BUSQUEDA EN EL OBJETO")
        # print(resultado)
    

    def crear(self, sentencia, datos):
        sql = 'INSERT INTO ' + sentencia
        self.cursor.execute(sql,datos)
        self.conexion.commit()
    
    def lectura(self, tabla):

        sql = "SELECT * FROM %s;" % (tabla)
        self.cursor.execute(sql)
        resultado = self.cursor.fetchall()
        return resultado

    #"SELECT * FROM tabla WHERE columna = dato;"

    def buscar(self, busqueda):
        
        sql = "SELECT * FROM %s;" % (busqueda)
        self.cursor.execute(sql)
        resultado = self.cursor.fetchall()
        return resultado

    def cambiar(self, sentencia, datos):
        
        sql = 'UPDATE ' + sentencia    
        print(sql)    
        print(datos)
        self.cursor.execute(sql, datos)
        actualizacion = self.conexion.commit()
        return actualizacion

    #'DELETE FROM nombre_tabla. WHERE nombre_columna = valor'
    def eliminar(self,sentencia):
        
        sql = 'DELETE FROM %s;' % (sentencia)
        eliminado = self.cursor.execute(sql)
        self.conexion.commit()
        return eliminado
        
