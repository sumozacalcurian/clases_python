# Clase Padre
class Personas(object): 
    def __init__(self, nombre, apellido):
        self.nombre = nombre
        self.apellido = apellido

#Clase para los adultos
class Representantes(Personas):
    def __init__(self, nombre, apellido, telefono, cedula):
        super().__init__(nombre, apellido)
        self.telefono = telefono
        self.cedula = cedula

#Clase para los niños
class Alumnos(Personas):
    def __init__(self, nombre, apellido, edad, representante_id):
        super().__init__(nombre, apellido)
        self.edad = edad
        self.representante_id = representante_id
