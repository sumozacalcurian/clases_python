import db
import os

from persona import Representantes
from persona import Alumnos

connect = db.Conexion('clases', 'postgres', 'root', '5432', '127.0.0.1')
#resultado = connect.muestra("alumnos")

class Funciones:
    def __init__(self):
        self.print_menu()

    def print_menu(self): #Menu de opciones
        dato = None
        while dato != "R" or dato != "A":
            print("Menu de Opciones")
            print("Escoja una opcion")
            print("[R]epresentantes")
            print("[A]lumnos")
            print("[S]alir")    
            dato = input("Ingrese un valor []: ")
            dato = dato.upper()
            if dato == "R":
                self.sec_menu("representantes")
            if dato == "A":
                self.sec_menu("alumnos")
            if dato == "S":
                os._exit(1)
            
    def sec_menu(self,tabla):
        print("¿Que necesita hacer?")
        print()
        print("[L]eer Todos")
        print("[I]nscribir")
        print("[C]ambiar Datos ")
        print("[B]uscar")
        print("[E]liminar")
        dato = input("Ingrese un valor []: ")
        dato = dato.upper()
        if dato == "L": #leer nuestro documento txt 
            print(connect.lectura(tabla))
        elif dato == "I": #insertar datos
            if tabla == 'representantes':
                adulto = Representantes(input("Ingrese nombre"), input("Ingrese apellido"), input("Ingrese telefono"), input("Ingrese cedula"))
                
                busqueda = connect.buscar("representantes WHERE cedula = %s" % adulto.cedula)

                if len(busqueda) == 0:
                    representante = (adulto.nombre, adulto.apellido, adulto.telefono, adulto.cedula)
                    
                    connect.crear('representantes(nombre,apellido,telefono,cedula) VALUES(%s,%s,%s,%s);', representante)
                    print("Representante registrado con éxito")
                    print()
                else:
                    print()
                    print("Este representante ya está registrado")
                    print(busqueda)
                    print()

            elif tabla == 'alumnos':
                nino = Alumnos(input("Ingrese nombre del Alumno"), input("Ingrese Apellido del Alumno"), input("Ingrese edad del Alumno"), input("Ingrese Cedula del Representante"))

                alumno = (nino.nombre, nino.apellido, nino.edad, nino.representante_id)
                
                connect.crear('alumnos(nombre,apellido,edad,representante_id) VALUES(%s,%s,%s,%s);', alumno)
            else:
                print("No Existe")
        elif dato == "C": # cambiar datos

            if tabla == 'representantes':
                busqueda = connect.buscar("representantes WHERE cedula = %s" % input("Ingrese la cédula del Representante: "))

                if len(busqueda) == 0:
                    print()
                    print("Representante no existente en la base de datos")
                    print()
                else:
                    print()
                    operacion = False

                    while operacion == False:
                        print("¿Que desea cambiar?")
                        print("[U]n campo en especifico.")
                        print("[T]odos los campos")
                        print("[R]egresar")
                        print()
                        respuesta = input("")
                        respuesta = respuesta.upper()

                        if respuesta == "U":
                            print("¿Que campo desea actualizar?")
                            print("[N]ombre del Representante")
                            print("[A]pellido del Representante")
                            print("[E]dad del Representante")
                            print("[C]edula del Representante")

                        elif respuesta == "T":
                            print("Vamos a modificar sus datos")
                            adulto = Representantes(input("Ingrese nombre del Representante: "), input("Ingrese apellido del Representante: "), input("Ingrese telefono del Representante: "), input("Ingrese cedula del Representante: "))

                            busquedaPrevia = "representantes WHERE cedula = %s" % adulto.cedula
                            busquedaPrevia = connect.buscar(busquedaPrevia)
                            if len(busquedaPrevia) != 0:
                                print("Esta cedula ya está registrada en la base de datos")
                                print()
                            else:
                                representante = (adulto.nombre, adulto.apellido, adulto.telefono, adulto.cedula)
                                indice = "WHERE id = %s" % (busqueda[0][0])
                                sql = tabla + " SET nombre=%s, apellido = %s, telefono=%s, cedula = %s " + indice

                                connect.cambiar(sql, representante)
                                print("Representante actualizado con éxito")
                                print()
                        elif respuesta == "R":
                            break
            elif tabla == 'alumnos':
                representante_id = input("Ingrese la cédula del Representante: ")
                busqueda = connect.buscar("alumnos WHERE representante_id = %s" % representante_id)

                print()
                if len(busqueda) == 0:
                    print("No hay alumnos registrados a este representante")
                else:

                    alumnoSeleccionado = None
                    respuestaAlumno = None
                    while respuestaAlumno == None:
                        print("Datos de los alumnos")
                        i = 0
                        for alumno in busqueda:
                            print("%s)" % (i + 1))
                            for dato in alumno:
                                print(dato)
                            i += 1
                            print()
                        
                        print()
                        alumnoSeleccionado = input("Seleccione al alumno a modificar: ")
                        print(alumnoSeleccionado)
                        if int(alumnoSeleccionado) > 0 and int(alumnoSeleccionado) <= len(busqueda):
                            print("Saliendo del ciclo")
                            break
                        else:
                            print("Valor invalido")
                    operacion = False

                    while operacion == False:
                        print("¿Que desea cambiar?")
                        print("[U]n campo en especifico.")
                        print("[T]odos los campos")
                        print("[R]egresar")
                        print()
                        respuesta = input("")
                        respuesta = respuesta.upper()

                        if respuesta == "U":
                            print("¿Que campo desea actualizar?")
                            print("[N]ombre del Alumno")
                            print("[A]pellido del Alumno")
                            print("[E]dad del Alumno")
                            print("[C]edula del Representante")
                                
                            individual = input("")
                            individual = individual.upper()
                            indice = "WHERE id = %s" % (busqueda[int(alumnoSeleccionado) - 1][0])
                            if individual == "N":
                                name = input("Ingrese el Nombre del Alumno: ")
                                sql = tabla + ' SET nombre=%s ' + indice
                                resultado = (name,)
                                connect.cambiar(sql,resultado)
                                print("Su Dato ha sido Actualizado")
                                print()
                                break
                            elif individual == "A":
                                apellido = input("Ingrese el Apellido del Alumno: ")
                                sql = tabla + ' SET apellido=%s ' + indice
                                resultado = (apellido,)
                                connect.cambiar(sql,resultado)
                                print("Su Dato ha sido Actualizado")
                                print()
                                break
                            elif individual == "E":
                                edad = input("Ingrese el Edad del Alumno: ")
                                sql = tabla + ' SET edad=%s ' + indice
                                resultado = (edad,)
                                connect.cambiar(sql,resultado)
                                print("Su Dato ha sido Actualizado")
                                print()
                                break
                            elif individual == "C":
                                cedula = input("Ingrese la cedula del Representante del Alumno: ")

                                busquedaPrevia = "representantes WHERE cedula = %s" % cedula
                                busquedaPrevia = connect.buscar(busquedaPrevia)
                                if len(busquedaPrevia) == 0:
                                    print("No hay ningun representante registrado con esta cedula")
                                    print()
                                else:
                                    sql = tabla + ' SET representante_id=%s ' + indice
                                    resultado = (cedula,)
                                    connect.cambiar(sql,resultado)
                                    print("Su Dato ha sido Actualizado")
                                    print()
                                    break
                            else:
                                print("Dato Invalido")
                        elif respuesta == "T":
                            
                            nino = Alumnos(input("Ingrese nombre del Alumno"), input("Ingrese Apellido del Alumno"), input("Ingrese edad del Alumno"), input("Ingrese la cedula del Representante"))

                            busquedaPrevia = "representantes WHERE cedula = %s" % nino.representante_id
                            busquedaPrevia = connect.buscar(busquedaPrevia)

                            if len(busquedaPrevia) == 0:
                                print("No hay ningun representante registrado con esta cedula")
                                print()
                            else:
                                alumno = (nino.nombre, nino.apellido, nino.edad, nino.representante_id)

                                indice = "WHERE id = %s" % (busqueda[int(alumnoSeleccionado) - 1][0])

                                sql = tabla + " SET nombre=%s, apellido = %s, edad=%s, representante_id = %s " + indice
                                print(sql)
                                connect.cambiar(sql, alumno)
                                print("Alumno actualizado con éxito")
                                print()
                                operacion = True
                                break
                        elif respuesta == "R":
                            break
                        else:
                            print("Valor invalido")
        elif dato == "B": #buscar datos
            sql = None
            busqueda = input("Ingrese Cedula del Representante") 
            if tabla == 'representantes':
                sql = tabla + ' WHERE cedula = %s' % busqueda
            elif tabla == 'alumnos':
                sql = tabla + ' WHERE representante_id = %s' % busqueda
            
            resultado = connect.buscar(sql)
            print()
            if len(resultado) == 0:
                print("Registro No encontrado")
            else:
                print("Registro encontrado")
                print(resultado)
            print()

        elif dato == "E": #eliminar datos
            sql = None
            sentencia = input("Ingrese Cedula del Representante") 
            if tabla == 'representantes':
                sql = tabla + ' WHERE cedula = %s' % sentencia
            elif tabla == 'alumnos':
                sql = tabla + ' WHERE representante_id = %s' % sentencia
            
            eliminado = connect.eliminar(sql)
            print (eliminado)
        elif dato == "S": #cerrar el archivo
            self.print_menu()
        else:
            print("Valor Invalido") #validacion de la opcion
        self.print_menu()


final = Funciones()